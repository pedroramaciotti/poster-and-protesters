##############################
# LOADING MODULES            #
##############################

# Basic data storing, manipulation, and analysis
import pandas as pd
import numpy as np
import scipy
from scipy.stats import poisson
from scipy.stats import pearsonr
from scipy.optimize import curve_fit
import statsmodels.api as sm
from statsmodels.tsa.stattools import grangercausalitytests as granger
from scipy.interpolate import make_interp_spline, BSpline

# Regular expression for pattern matching in text classification
import re

# Additional packages for formatting
import itertools
from datetime import timedelta, date
import calendar
from roman_numerals import LOWERCASE, convert_to_numeral

# Graphic tools
import matplotlib.pyplot as plt
import seaborn as sn
from matplotlib.lines import Line2D
import matplotlib.dates as mdates
from matplotlib.ticker import FixedLocator, FixedFormatter
from matplotlib.dates import MonthLocator
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mtick

##############################
# FUNCTIONS                  #
##############################

def get_mask(x,y):
    mask_nan = np.logical_or(np.isnan(x),np.isnan(y)) 
    mask_inf = np.logical_or(np.isinf(x),np.isinf(y)) 
    return ~np.logical_or(mask_nan,mask_inf);

def spliner(x,y,n,k):
    xnew = np.linspace(x.min(),x.max(),n)
    spl = make_interp_spline(x, y, k=k)
    ynew = spl(xnew)
    return xnew,ynew;

def linfunc(t,a,b):
    return a+b*t;
    

##############################
# LOADING DATA               #
##############################


folder = ...

# FB posts logs
log_df=pd.read_csv(folder+'/posts_log.csv')
columns= {'account_id':str,'date':str,'region_code':str,'department_code':str,
          'actual_like_count':int,'actual_share_count':int,'actual_comment_count':int,
          'F_logistic':bool, 'F_gj':bool, 'F_precarity':bool, 'F_ric':bool, 'F_fuel':bool,'N_flags':int}
log_df = log_df.astype(dtype=columns,errors='ignore')
log_df['department_code'] = log_df['department_code'].astype(int,errors='ignore')
flag_cols = [c for c in log_df.columns if c.startswith('F_')]

# Geographical data
communes_df=pd.read_csv(folder+'/geodata_communes.csv')
columns={'commune_code':str, 'department_code':str, 'region_code':str,'population':int}
communes_df=communes_df.astype(dtype=columns,errors='ignore')

departments_df=pd.read_csv(folder+'/geodata_departments.csv')
columns={'department_code':str, 'region_code':str,'population':int}
departments_df=departments_df.astype(dtype=columns,errors='ignore')

regions_df=pd.read_csv(folder+'/geodata_regions.csv')
columns={'region_code':str,'population':int}
regions_df=regions_df.astype(dtype=columns,errors='ignore')

actes_df=pd.read_csv(folder+'/actes_data.csv')

columns={'department_code':str,}
groups_geo_df=pd.read_csv(folder+'/geodata_groups.csv').astype(dtype=columns,errors='ignore')

# gathering from www.gilets-jaunes.com
rassemblements_df=pd.read_csv(folder+'/gatherings.csv')
fb_groups_in_rassemblements = list(set(list(itertools.chain.from_iterable(rassemblements_df['fb_groups'].values.tolist()))))
fb_events_in_rassemblements = list(set(list(itertools.chain.from_iterable(rassemblements_df['fb_events'].values.tolist()))))


group_geo_df=pd.read_csv(folder+'/geodata_groups.csv')

#############################
# NEW COLUMNS               #
#############################

actes_df['datetime'] = actes_df.date.apply(pd.Timestamp)
log_df['date'] = pd.to_datetime(log_df['date'])

#############################
# COMPUTING TIME SERIES     #
#############################

posts_time_series = {}
for period in ['1d','W','M']:
    posts_time_series[period]=log_df['date'].value_counts().resample(period).sum()

groups_time_series = {}
for period in ['1d','W','M']:
    groups_time_series[period]=log_df.set_index('date').account_id.resample(period).nunique()
    
comments_time_series = {}
for period in ['1d','W','M']:
    comments_time_series[period]=log_df[['date','actual_comment_count']].groupby('date').sum().resample(period).sum()

likes_time_series = {}
for period in ['1d','W','M']:
    likes_time_series[period]=log_df[['date','actual_like_count']].groupby('date').sum().resample(period).sum()

shares_time_series = {}
for period in ['1d','W','M']:
    shares_time_series[period]=log_df[['date','actual_share_count']].groupby('date').sum().resample(period).sum()

##############################
# LOG PER DATEGROUPS         #,
##############################


dategroups = {
              1:('2018-10-01','2018-11-16'), # Previous to 17 nov 2018
              2:('2018-10-18','2019-01-31'), # Immediate Post 17 nov 2018
              3:('2018-10-10','2018-12-15'), # Comparison with Boyer et al. 2020
              4:('2018-8-30','2019-07-30'),  # For time series of flags
              5:('2018-11-10','2018-11-16'), # For time series of flags
              # 
              17:('2018-11-10','2018-11-17'),
              }   

lg_dg = {}
for dg,limit_dates in dategroups.items():
    lg_dg[dg] = log_df[(log_df.date>=pd.Timestamp(limit_dates[0]))&(log_df.date<=pd.Timestamp(limit_dates[1]))]
    
g_before_17nov2018 = log_df[(log_df.date<=pd.Timestamp('2018-11-16'))].account_id.unique().tolist()
g_after_17nov2018 = log_df[~log_df.account_id.isin(g_before_17nov2018)].account_id.unique().tolist()
intersection = np.intersect1d(fb_groups_in_rassemblements,log_df.account_id.values)

    
##############################
# EMBEDDING: DEPARTMENTS     #
##############################

emb_dept = pd.DataFrame()
emb_dept['department_code'] = departments_df['department_code'].copy(deep=True)
emb_dept['population'] = departments_df['population']
emb_dept['skm'] = departments_df['skm']
emb_dept['density'] = emb_dept.apply(lambda row: row.population/row.skm,axis=1)

# N_rassemblements
emb_dept['R'] = emb_dept['department_code'].map(rassemblements_df.department_code.value_counts())
emb_dept['R_PH'] = emb_dept.apply(lambda row: row.R/row.population, axis=1)
emb_dept['R_PSKM'] = emb_dept.apply(lambda row: row.R/row.skm, axis=1)

# Facebook Events
emb_dept['CGJE'] = emb_dept['department_code'].map(rassemblements_df[['department_code','N_fb_events']].groupby('department_code').sum()['N_fb_events'])
emb_dept['CGJE_PH'] = emb_dept.apply(lambda row: row.CGJE/row.population, axis=1)
emb_dept['CGJE_PSKM'] = emb_dept.apply(lambda row: row.CGJE/row.skm, axis=1)

# Facebook Carte Jaune groups
emb_dept['CGJG'] = emb_dept['department_code'].map(rassemblements_df[['department_code','N_fb_groups']].groupby('department_code').sum()['N_fb_groups'])
emb_dept['CGJG_PH'] = emb_dept.apply(lambda row: row.CGJG/row.population, axis=1)
emb_dept['CGJG_PSKM'] = emb_dept.apply(lambda row: row.CGJG/row.skm, axis=1)

# Total CarteJaune items: N_eventsgroups
emb_dept['CGJI'] = emb_dept['department_code'].map(rassemblements_df[['department_code','N_eventsgroups']].groupby('department_code').sum()['N_eventsgroups'])
emb_dept['CGJI_PH'] = emb_dept.apply(lambda row: row.CGJI/row.population, axis=1)
emb_dept['CGJI_PSKM'] = emb_dept.apply(lambda row: row.CGJI/row.skm, axis=1)

# Naming convention: 
# dg0/dg1/gd2/dg3/
# posts,groups,comments,likes : P/G/C/L
# Flag: All, F_gj, 
# per_hab / per_skm : Tot PH PSKM
name = lambda dg,q,f,n: 'DG%d_%s_%s_%s'%(dg,q,f,n)

# Total groups
emb_dept['DG1_G_complement'] = emb_dept['department_code'].map(lg_dg[1].loc[~lg_dg[1].account_id.isin(intersection),['department_code','account_id']].groupby('department_code').nunique()['account_id'])


emb_dept['DG1_Total_Groups'] = emb_dept.apply(lambda row: row.DG1_G_complement+row.CGJG, axis=1)
emb_dept['DG1_Total_Groups_PH'] = emb_dept.apply(lambda row: row.DG1_Total_Groups/row.population, axis=1)
emb_dept['DG1_Total_Groups_PSKM'] = emb_dept.apply(lambda row: row.DG1_Total_Groups/row.skm, axis=1)

emb_dept['DG1_Total_GE'] = emb_dept.apply(lambda row: row.DG1_Total_Groups+row.CGJE, axis=1)
emb_dept['DG1_Total_GE_PH'] = emb_dept.apply(lambda row: row.DG1_Total_Groups/row.population, axis=1)
emb_dept['DG1_Total_GE_PSKM'] = emb_dept.apply(lambda row: row.DG1_Total_Groups/row.skm, axis=1)

emb_dept.fillna(value=0.0,inplace=True)


##############################
# EMBEDDING: ACTES           #
##############################

groups_first_post = log_df[['account_id','date']].groupby('account_id').min()
groups_first_post['date'] =groups_first_post['date'].apply(pd.Timestamp)

foreign_groups = group_geo_df[group_geo_df.department=='OTHER'].account_id
national_mask = ~log_df.account_id.isin(foreign_groups)

emb_actes = pd.DataFrame()

emb_actes['acte']   = actes_df['acte']
emb_actes['people'] = actes_df['count']
emb_actes['date']   = actes_df['date'] 
emb_actes['start']  = actes_df['start'] 
emb_actes['end']    = actes_df['end'] 

for ix,row in emb_actes.iterrows():
    acte_log = log_df[(log_df.date>=row.start)&(log_df.date<=row.end)&(~log_df.account_id.isin(foreign_groups))]
    emb_actes.loc[ix,'G_All'] = acte_log.account_id.unique().size
    emb_actes.loc[ix,'P_All'] = acte_log.shape[0]
    emb_actes.loc[ix,'C_All'] = acte_log.actual_comment_count.sum()
    emb_actes.loc[ix,'L_All'] = acte_log.actual_like_count.sum()
    emb_actes.loc[ix,'S_All'] = acte_log.actual_share_count.sum()
    emb_actes.loc[ix,'NG_All'] = groups_first_post[(groups_first_post.date.apply(pd.Timestamp)>=row.start) & (groups_first_post.date.apply(pd.Timestamp)<=row.end)].size 
    for f in flag_cols:
        emb_actes.loc[ix,'G_'+f] = acte_log[acte_log[f]].account_id.unique().size
        emb_actes.loc[ix,'P_'+f] = acte_log[acte_log[f]].shape[0]
        emb_actes.loc[ix,'C_'+f] = acte_log[acte_log[f]].actual_comment_count.sum()
        emb_actes.loc[ix,'L_'+f] = acte_log[acte_log[f]].actual_like_count.sum()
        emb_actes.loc[ix,'S_'+f] = acte_log[acte_log[f]].actual_share_count.sum()
    # from Caterina's 8 apr email
    f='F_logistic_GJ'
    emb_actes.loc[ix,'G_'+f] = acte_log[acte_log['F_logistic']|acte_log['F_gj']].account_id.unique().size
    emb_actes.loc[ix,'P_'+f] = acte_log[acte_log['F_logistic']|acte_log['F_gj']].shape[0]
    emb_actes.loc[ix,'C_'+f] = acte_log[acte_log['F_logistic']|acte_log['F_gj']].actual_comment_count.sum()
    emb_actes.loc[ix,'L_'+f] = acte_log[acte_log['F_logistic']|acte_log['F_gj']].actual_like_count.sum()
    emb_actes.loc[ix,'S_'+f] = acte_log[acte_log['F_logistic']|acte_log['F_gj']].actual_share_count.sum()
    f='F_not_logistic_GJ'
    emb_actes.loc[ix,'G_'+f] = acte_log[~(acte_log['F_logistic']|acte_log['F_gj'])].account_id.unique().size
    emb_actes.loc[ix,'P_'+f] = acte_log[~(acte_log['F_logistic']|acte_log['F_gj'])].shape[0]
    emb_actes.loc[ix,'C_'+f] = acte_log[~(acte_log['F_logistic']|acte_log['F_gj'])].actual_comment_count.sum()
    emb_actes.loc[ix,'L_'+f] = acte_log[~(acte_log['F_logistic']|acte_log['F_gj'])].actual_like_count.sum()
    emb_actes.loc[ix,'S_'+f] = acte_log[~(acte_log['F_logistic']|acte_log['F_gj'])].actual_share_count.sum()
    f='F_fuel_precarity_ric'
    emb_actes.loc[ix,'G_'+f] = acte_log[(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].account_id.unique().size
    emb_actes.loc[ix,'P_'+f] = acte_log[(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].shape[0]
    emb_actes.loc[ix,'C_'+f] = acte_log[(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].actual_comment_count.sum()
    emb_actes.loc[ix,'L_'+f] = acte_log[(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].actual_like_count.sum()
    emb_actes.loc[ix,'S_'+f] = acte_log[(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].actual_share_count.sum()
    f='F_not_fuel_precarity_ric'
    emb_actes.loc[ix,'G_'+f] = acte_log[~(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].account_id.unique().size
    emb_actes.loc[ix,'P_'+f] = acte_log[~(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].shape[0]
    emb_actes.loc[ix,'C_'+f] = acte_log[~(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].actual_comment_count.sum()
    emb_actes.loc[ix,'L_'+f] = acte_log[~(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].actual_like_count.sum()
    emb_actes.loc[ix,'S_'+f] = acte_log[~(acte_log['F_fuel']|acte_log['F_ric']|acte_log['F_precarity'])].actual_share_count.sum()
    f='F_no_f'
    emb_actes.loc[ix,'G_'+f] = acte_log[acte_log.N_flags<1].account_id.unique().size
    emb_actes.loc[ix,'P_'+f] = acte_log[acte_log.N_flags<1].shape[0]
    emb_actes.loc[ix,'C_'+f] = acte_log[acte_log.N_flags<1].actual_comment_count.sum()
    emb_actes.loc[ix,'L_'+f] = acte_log[acte_log.N_flags<1].actual_like_count.sum()
    emb_actes.loc[ix,'S_'+f] = acte_log[acte_log.N_flags<1].actual_share_count.sum()
    
    
    
    
####################################
# PLOTTING PROTESTERS VS ACTES     #
####################################
    
fig = plt.figure(figsize=(9, 4))
ax=fig.add_subplot(1,1,1)
ax.bar(range(actes_df.shape[0]),actes_df['count'].values,color='red')
# xticks
ax.set_xticks(range(actes_df.shape[0]))
ax.set_xticklabels(actes_df.datetime.dt.date.values,rotation = 45, ha="right",fontsize=13)
# yticks
ax.set_yticks([0,50000,100000,150000,200000,250000,300000])
ax.set_yticklabels(['0K','50K','100K','150K','200K','250K','300K'],fontsize=14)
axb = ax.twiny()
axb.set_xlim(ax.get_xlim())
axb.set_xticks(range(actes_df.shape[0]))
axb.set_xticklabels(actes_df.roman.values,rotation = 45, ha="left",fontsize=14)
# labels and titles
ax.set_ylabel('Participants',fontsize=14)
ax.set_title('Actes',fontsize=14)
ax.locator_params(axis='x', nbins=15)
plt.tight_layout()
plt.savefig('Figures/actes_protester_bars_bis.pdf')
plt.clf()
plt.close()

##############################################
# GROUPES PER NUMBER OF MEMBERS              #
##############################################

group_sizes=log_df.sort_values(by='date',ascending=True).drop_duplicates(subset='account_id',keep='last').loc[:,['date','account_id','account_subscriber_count']]
group_sizes.sort_values(by='account_subscriber_count',ascending=False,inplace=True)
group_posts = log_df['account_id'].value_counts().sort_values(ascending=False)

fig = plt.figure(figsize=(9, 3))
ax=fig.add_subplot(1,1,1)
ax.bar(np.arange(group_sizes.shape[0]),group_sizes.account_subscriber_count,width=1,color='orange')
ax.set_yscale('log')
# ax.set_xscale('log')
ax.set_xticks([0,200,400,600,891])
ax.set_xticklabels(['1st','200th','400th','600th','892th'])
ax.set_ylim((1e0,1e6))
ax.set_xlim((0,891))
ax.set_xlabel('Groups ordered by member count')
ax.set_ylabel('Group size')
plt.tight_layout()
plt.savefig('Figures/group_sizes.pdf')
plt.clf()
plt.close()

fig = plt.figure(figsize=(4, 4))
ax=fig.add_subplot(1,1,1)
ax.plot(np.arange(group_sizes.shape[0])+1,group_sizes.account_subscriber_count,'+',color='blue')
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xticks([1,10,100,892])
ax.set_xticklabels(['1st','10th','100th','892th'])
ax.set_ylim((1e0,1e6))
ax.set_xlim((0.9,1e3))
ax.set_xlabel('Groups rank')
ax.set_ylabel('Group size')
ax.grid(True)
left, bottom, width, height = [0.3, 0.25, 0.4, 0.3]
ax2 = fig.add_axes([left, bottom, width, height])
ax2.plot(np.arange(group_posts.size)+1,group_posts.values,'+',color='blue')
ax2.set_yscale('log')
ax2.set_xscale('log')
ax2.set_xticks([1,10,100,892])
ax2.set_xticklabels(['1st','10th','100th','892th'])
ax2.set_ylim((1e0,1e7))
ax2.set_xlim((0.9,1e3))
ax2.text(5,1e6,'number of posts')
plt.tight_layout()
plt.savefig('Figures/group_sizes_bis.pdf')
plt.clf()
plt.close()

##############################################
# PLOTTING SUMMARY DAILY ONLINE ACTIVITY     #
##############################################

periods = ['1d','W']
pretty_periods = {'1d':'Daily','W':'Weekly','M':'Monthly'}
months = mdates.MonthLocator('%Y')

def change_repeated(L):
    counter=0
    last=''
    while counter<len(L):
        current=L[counter]
        if current==last:
            L[counter]=''
        last=current
        counter+=1
    return L;
        
# All timeseries (daily) in one figure: groups, posts, likes, comments
def normalize(date_string):
    splitted = date_string.split('-')
    if splitted[1].startswith('0'):
        splitted[1] = splitted[1][1:]
    if splitted[2].startswith('0'):
        splitted[2] = splitted[2][1:]
    return '-'.join(splitted);
N_days = groups_time_series['1d'].size    
dates_list = groups_time_series['1d'].index.tolist()
dates_list = ['%d-%d-%d'%(i.year,i.month,i.day) for i in dates_list]
period='1d'
days_offset=300
fs=12
ylabelspace=-0.06
fig = plt.figure(figsize=(12, 5))
# tweaking space between subplots
plt.subplots_adjust(wspace=0.2,top=0.9,bottom=0.1)
ax={'Groups':fig.add_subplot(5,1,1),'Posts':fig.add_subplot(5,1,2),
    'Comments':fig.add_subplot(5,1,3),'Likes':fig.add_subplot(5,1,4),
    'Shares':fig.add_subplot(5,1,5),
    }
groups_time_series[period].plot.bar(linewidth=1,style='-o',ax=ax['Groups'])
posts_time_series[period].plot.bar(linewidth=1,style='-o',ax=ax['Posts'])
comments_time_series[period].plot.bar(linewidth=1,style='-o',ax=ax['Comments'])
likes_time_series[period].plot.bar(linewidth=1,style='-o',ax=ax['Likes'])
shares_time_series[period].plot.bar(linewidth=1,style='-o',ax=ax['Shares'])
for a in ['Groups','Posts','Comments','Likes','Shares']:
    ax[a].set_xlabel('')
    ax[a].tick_params(axis='x',which='both',bottom=False,top=False,labelbottom=False)
    # actes
    ax[a].axvline(dates_list.index('2018-11-17'),color='red',linewidth=1.5,alpha=0.5)
    for acte_date in actes_df.date[1:]:
        ax[a].axvline(dates_list.index(normalize(acte_date)),color='red',linewidth=1.5,alpha=0.3)
# ax['Shares'].text(dates_list.index('2018-11-17')-10,150000,'17 Nov 2018',
#                  rotation=90,color='red',fontsize=8)
# y labels
ax['Groups'].set_ylabel('Groups',fontsize=fs)
ax['Posts'].set_ylabel('Posts',fontsize=fs)
ax['Comments'].set_ylabel('Comments',fontsize=fs)
ax['Likes'].set_ylabel('Likes',fontsize=fs)
ax['Shares'].set_ylabel('Shares',fontsize=fs)
#
ax['Likes'].get_legend().remove()
ax['Comments'].get_legend().remove() 
ax['Shares'].get_legend().remove()    
#
ax['Shares'].tick_params(axis='x',which='both',bottom=True,top=False,labelbottom=True)    
ax['Shares'].set_xticks(range(groups_time_series['1d'].size))
ax['Shares'].set_xticklabels(change_repeated(['%d-%d'%(i.year,i.month) for i in groups_time_series['1d'].index]),
                            rotation = 25, ha="right",fontsize=fs)
# y axes
ax['Groups'].set_yticks([0,300,600,900,1000])
ax['Groups'].set_yticklabels(labels=['0','300','600','900',''],fontsize=fs-2)
ax['Posts'].set_yticks([0,30000,60000,90000])
ax['Posts'].set_yticklabels(['0K','30K','60K','90K'],fontsize=fs-2)
ax['Comments'].set_yticks([0,300000,600000])
ax['Comments'].set_yticklabels(['0M','0.3M','0.6M'],fontsize=fs-2)
ax['Likes'].set_yticks([0,500000,1000000])
ax['Likes'].set_yticklabels(['0M','0.5M','1.0M'],fontsize=fs-2)
ax['Shares'].set_yticks([0,1000000,2000000])
ax['Shares'].set_yticklabels(['0M','1.0M','2.0M'],fontsize=fs-2)
# actes
ax['GroupsBis']=ax['Groups'].twiny()
ax['GroupsBis'].set_xlim(ax['Groups'].get_xlim())
ax['GroupsBis'].set_xticks([dates_list.index(normalize(d)) for d in actes_df.date.values])
ax['GroupsBis'].set_xticklabels(actes_df.roman.values,rotation = 45, ha="left",fontsize=fs)
for a in ['Groups','Posts','Comments','Likes','Shares']:
    ax[a].set_xlim((days_offset+30,days_offset+30+300))
ax['GroupsBis'].set_xlim(ax['Groups'].get_xlim())

# fixing ylabel coords
ax['Groups'].get_yaxis().set_label_coords(ylabelspace,0.5)
ax['Posts'].get_yaxis().set_label_coords(ylabelspace,0.5)
ax['Comments'].get_yaxis().set_label_coords(ylabelspace,0.5)
ax['Likes'].get_yaxis().set_label_coords(ylabelspace,0.5)
ax['Shares'].get_yaxis().set_label_coords(ylabelspace,0.5)

plt.savefig('Figures/All_daily_timeseries.pdf')
plt.clf()
plt.close()

##############################################
# STAGES                                     #
##############################################
  
groups_first_post = log_df[['account_id','date']].groupby('account_id').min()
groups_first_post['date'] =groups_first_post['date'].apply(pd.Timestamp)
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)
start_date = date(2018, 8, 30)
end_date = date(2019, 4, 1)
dates=[]
cum_N_groups = []
active_groups = []
for single_date in daterange(start_date, end_date):
    dates.append(normalize(single_date.strftime("%Y-%m-%d")))
    cum_N_groups.append(groups_first_post[groups_first_post.date<=pd.Timestamp(single_date.strftime("%Y-%m-%d"))].index.unique().size)
    active_groups.append(groups_time_series['1d'][single_date.strftime("%Y-%m-%d")])
# dates for xticks
datesxticks = dates.copy()
for i in range(len(dates)):
    y = int(dates[i].split('-')[0])
    m = int(dates[i].split('-')[1])
    d = int(dates[i].split('-')[2])
    if (d!=1 and d!=15):# and d!=calendar.monthrange(y, m)[1]):
        datesxticks[i]=''
# stages
fs=7
fig = plt.figure(figsize=(8, 2.0))
ax=fig.add_subplot(1,1,1)
ax.plot(range(len(dates)),cum_N_groups,color='k',linestyle='--')
ax.plot(range(len(dates)),active_groups,color='k')
ax.set_xticks(range(len(dates)))
ax.set_xticklabels(datesxticks,rotation = 25, ha="right",fontsize=fs)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(fs) 
ax.set_ylabel('Groups',fontsize=fs)
axb=ax.twiny()
axb.set_xlim(ax.get_xlim())
# Vertical bars and zones
special_dates = ['2018-10-22','2018-11-17','2019-1-19']
for sd in special_dates:
    ax.axvline(dates.index(sd),color='black',linewidth=1.5,alpha=1)
ax.axvspan(0, dates.index(special_dates[0]), alpha=0.3, color='red')
ax.axvspan(dates.index(special_dates[0]), dates.index(special_dates[1]), alpha=0.3, color='blue')
ax.axvspan(dates.index(special_dates[1]), dates.index(special_dates[2]), alpha=0.3, color='red')
ax.axvspan(dates.index(special_dates[2]),len(cum_N_groups), alpha=0.3, color='blue')
#
text_heigh=425
ax.text(dates.index(special_dates[0])*0.5-3,text_heigh,r'$\Phi_1$',fontsize=fs+2)
ax.text((dates.index(special_dates[0])+dates.index(special_dates[1]))*0.5-3,text_heigh,r'$\Phi_2$',fontsize=fs+2)
ax.text((dates.index(special_dates[1])+dates.index(special_dates[2]))*0.5-3,text_heigh,r'$\Phi_3$',fontsize=fs+2)
ax.text((dates.index(special_dates[2])+len(cum_N_groups))*0.5-3,text_heigh,r'$\Phi_4$',fontsize=fs+2)
# Setting Twin xticks and xticklabels
axb.set_xticks([dates.index(d) for d in special_dates])
axb.set_xticklabels(['2018-10-23\n','2018-11-17\n(Acte I)','2019-1-19\n(Acte X)'],fontsize=fs)
# legend
ax.legend(['Created','Active'],fontsize=fs,loc='lower right')
#
ax.set_xlim((0,len(cum_N_groups)))
axb.set_xlim((0,len(cum_N_groups)))
ax.set_ylim((0,900))
ax.set_yticks([0,300,600,900])
plt.tight_layout()
plt.savefig('Figures/stages.pdf')
plt.clf()
plt.close()

#################################################
# CORRELATION CARTE GILETS JAUNES VS BLOCKAGES  #
#################################################


emb_dept['aux']=1000*emb_dept['CGJI_PH']
emb_dept[['department_code','aux']].to_csv('CGJI_PH.csv',float_format='%.3f',sep=',',index=False)


x_col='CGJI_PH'
y_col='R_PH'
x=1000*emb_dept[x_col]
y=1000*emb_dept[y_col]
mask=get_mask(x,y)
r,p=pearsonr(x[mask],y[mask])
custom_legend=[
                Line2D([0],[0],color='white',lw=2,alpha=1.0, label='Pearson corr. = %.3f'%r),
              ]
if p<0.001:
    custom_legend.append(Line2D([0],[0],color='white',lw=2,alpha=1.0, label='p-value <0.001'))
else:
    custom_legend.append(Line2D([0],[0],color='white',lw=2,alpha=1.0, label='p-value =%.3f'%p))
# regression
def linear(x,a,b):
    return a+b*x;
popt,_ = curve_fit(linear,x,y)
est = sm.OLS(y, x).fit()
x_line = np.linspace(0,x.max())
y_line = x_line*est.params[0]
# outliers
sub_df=emb_dept[['department_code',x_col,y_col]].sort_values(by=y_col,ascending=False)  
outliers=['52', '09', '24','08', '05']
outlier_df=sub_df[sub_df.department_code.isin(outliers)]
# two examples
sub_df['y_dif']=sub_df.apply(lambda row: np.abs(row[y_col]-est.params[0]*row[x_col]),axis=1)
sub_df.sort_values(by='y_dif',ascending=True,inplace=True)
two_examples=['31','47']
examples_df=sub_df[sub_df.department_code.isin(two_examples)]
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
fs=14
fig = plt.figure(figsize=(5.5, 7.5)) # originaly 7,6
ax = plt.subplot(1,1,1)
ax.scatter(x,y,alpha=0.5,color='blue')
ax.set_xlim((-0.0025,0.045))
ax.set_ylim((-0.0025,0.085))
ax.set_xlabel('Groups and events per 1000 inhab.',fontsize=fs)
ax.set_ylabel('Blockades and gatherings per 1000 inhab.',fontsize=fs)
ax.legend(handles=custom_legend)
# outliers
for ix,row in outlier_df.iterrows():
    name= departments_df.loc[departments_df.department_code==row.department_code,'department_name'].iloc[0]
    if row.department_code=='05':
        adjust=-0.001
    else:
        adjust=0
    ax.text(row[x_col]*1000+0.001,row.R_PH*1000+adjust+0.0005,'Dept. %s: %s'%(row.department_code,name), fontsize=fs-2)
# regression line
ax.plot(x_line,y_line,color='red') 
# Two examples
for ix,row in examples_df.iterrows():
    if ix==31:
        adjust=-0.001
    else:
        adjust= 0
    ax.scatter(row[x_col]*1000,row.R_PH*1000,alpha=0.75,facecolors='yellow',edgecolors='blue',linewidths=1.5)
    name= departments_df.loc[departments_df.department_code==row.department_code,'department_name'].iloc[0]
    textstr = '\n'.join((
        'Dept. %s: %s'%(row.department_code,name),
        'Pop.: %d'%departments_df[departments_df.department_code==row.department_code].population.iloc[0],
        'Groups & Events: %d'%emb_dept[emb_dept.department_code==row.department_code].CGJI.iloc[0],
        'Dems. & Road-blocks: %d'%emb_dept[emb_dept.department_code==row.department_code].R.iloc[0],))
    ax.text(row[x_col]*1000+0.015, row.R_PH*1000+0.005+adjust, textstr,# transform=ax.transAxes, 
            fontsize=10,verticalalignment='top', bbox=props)
# arrow
ax.arrow(0.019, 0.0066, -0.0095, 0,alpha=0.5)
ax.arrow(0.026725, 0.018, -0.0095, 0,alpha=0.5)
#
for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(fs) 
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(fs) 
plt.tight_layout()
plt.savefig('Figures/CGJ_R.pdf')
plt.clf()
plt.close()

##############################################
# PLOTTING CUMULATIVE NUMBER OF GROUPS       #
# Sub and Supra Critical                     #
##############################################

# creating the signal
groups_first_post = log_df[['account_id','date']].groupby('account_id').min()
groups_first_post['date'] =groups_first_post['date'].apply(pd.Timestamp)
groups_first_post['days_from_oct1st'] = groups_first_post['date'].apply(lambda x: (x - pd.Timestamp('2018-10-1')).days)
signal = groups_first_post['days_from_oct1st'].value_counts().sort_index()
signal = signal[signal.index>=0]
signal = signal.reindex(list(range(signal.index.min(),signal.index.max()+1)),fill_value=0)
cumsignal = signal.cumsum()
# critical dates and stages
tc_S2 = 22 # Oct 23th, 2018
tc_S3 = 46 # Nov 17th, 2018
signal_S2 = cumsignal[(signal.index>=tc_S2)&(signal.index<=tc_S3)]
signal_S3 = cumsignal[(signal.index>=tc_S3)]
cumsignal_S2 = cumsignal[(cumsignal.index>tc_S2)&(cumsignal.index<=tc_S3)]
cumsignal_S3 = cumsignal[(cumsignal.index>tc_S3)]
# dates array
dates = [(pd.Timestamp('2018-9-30')+timedelta(days=x)).strftime('%Y-%m-%d') for x in signal.index]
# curve fitting
def AcumS2(t,C,theta):
    return cumsignal_S2.min()+C*np.power(t-cumsignal_S2.index.min(),theta)/theta;
def AcumS3(t,C,theta):
    return cumsignal_S3.min()+C*np.power(t-cumsignal_S3.index.min(),theta)/theta;
def AcumS3exp(t,C,alpha):
    return cumsignal_S3.min()+C*(1-np.exp(-alpha*(t-cumsignal_S3.index.min())));
def AS2(t,C,theta):
    return C*np.power(t-signal_S2.index.min(),-1+theta);
def AS3(t,C,theta):
    return C*np.power(t-signal_S3.index.min(),-1+theta-1);
def AS3exp(t,C,alpha):
    return C*alpha*np.exp(-alpha*(t-signal_S3.index.min()));
Ctheta_S2,pcov_S2 = curve_fit(AcumS2,cumsignal_S2.index.values[1:],cumsignal_S2.values[1:])
Ctheta_S3,pcov_S3 = curve_fit(AcumS3,cumsignal_S3.index.values[1:],cumsignal_S3.values[1:])
Calpha_S3,pcov_S3 = curve_fit(AcumS3exp,cumsignal_S3.index.values[1:],cumsignal_S3.values[1:])
fit_cum_S2 = AcumS2(cumsignal_S2.index.values,Ctheta_S2[0],Ctheta_S2[1])
fit_cum_S3 = AcumS3(cumsignal_S3.index.values,Ctheta_S3[0],Ctheta_S3[1])
fit_cum_S3exp = AcumS3exp(cumsignal_S3.index.values,Calpha_S3[0],Calpha_S3[1])
fit_S2 = AS2(signal_S2.index.values,Ctheta_S2[0],Ctheta_S2[1])
fit_S3 = AS3(signal_S3.index.values,Ctheta_S3[0],Ctheta_S3[1])
fit_S3exp = AS3exp(signal_S3.index.values,Calpha_S3[0],Calpha_S3[1])
# Plotting
S2scr_color='blue'
S3scr_color='blue'
S3exp_color='red'
fs=12
fig = plt.figure(figsize=(14, 5))
# axis 1: cummulative
ax1=fig.add_subplot(1,2,1)
ax1.plot(cumsignal.index,cumsignal.values,color='k',alpha=0.5)
ax1.plot(cumsignal_S2.index,fit_cum_S2,color=S2scr_color,linestyle='--')
ax1.plot(cumsignal_S3.index,fit_cum_S3,color=S3scr_color,linestyle='dotted')
ax1.plot(cumsignal_S3.index,fit_cum_S3exp,color=S3exp_color,linestyle='dotted')
# 
ax1.set_ylim((-10,1000))
ax1.set_ylabel('Cumulative number of groups',fontsize=fs+1)
ax1.axvline(tc_S2+1,color='k',linestyle=':')
ax1.axvline(tc_S3+1,color='k',linestyle=':')
ax1.set_xticks(signal.index.values)
ax1.set_xticklabels(dates,rotation=35,ha='right',fontsize=fs)
ax1.locator_params(axis='x', nbins=10)
ax1.legend(['Cumulative nº of groups','Best fit '+r'$\Phi_2$'+' (power-law)','Best fit '+r'$\Phi_3$'+' (power-law)','Best fit '+r'$\Phi_3$'+' (exponential)'])
ax1.text(tc_S2-10,925,r'$\Phi_1$',fontsize=fs)
ax1.text(0.5*(tc_S2+tc_S3)-2,925,r'$\Phi_2$',fontsize=fs)
ax1.text(tc_S3+5,925,r'$\Phi_3$',fontsize=fs)
scS2=ax1.text(5,120,r'A='+'%.2f'%Ctheta_S2[0]+'\n'+r'$\theta=$'+'%.2f'%Ctheta_S2[1],color=S2scr_color)
scS2.set_bbox(dict(facecolor=S2scr_color, alpha=0.1, edgecolor=S2scr_color))
scS3=ax1.text(29,500,r'A='+'%.2f'%Ctheta_S3[0]+'\n'+r'$\theta=$'+'%.2f'%Ctheta_S3[1],color=S3scr_color)
scS3.set_bbox(dict(facecolor=S3scr_color, alpha=0.1, edgecolor=S3scr_color))
exS3=ax1.text(55,300,r'B='+'%.2f'%Calpha_S3[0]+'\n'+r'$\alpha=$'+'%.2f'%Calpha_S3[1],color=S3exp_color)
exS3.set_bbox(dict(facecolor=S3exp_color, alpha=0.1, edgecolor=S3exp_color))
ax1.text(0.7*signal.size,1010*0.6,
         'Power law:\n\n'+ r'$F(x)=C+\frac{A}{\theta}\left(t-t_c \right)^{\theta}$',color='blue')
ax1.text(0.7*signal.size,1010*0.4,
         'Exponential:\n\n'+r'$F(x)=C+B\left(1-e^{-\alpha (t-t_c)}\right)$',color='red')
ax1.text(tc_S2-3,1010*0.65,'23 Oct Shock',rotation=90)
ax1.text(tc_S3-3,1010*0.65,'17 Nov Shock',rotation=90)
# axis 2: signal
ax2=fig.add_subplot(1,2,2)
ax2.plot(signal.index,signal.values,color='k',alpha=0.5)
ax2.plot(signal_S2.index,fit_S2,color=S2scr_color,linestyle='--')
ax2.plot(signal_S3.index,fit_S3,color=S3scr_color,linestyle='dotted')
ax2.plot(signal_S3.index[1:],fit_S3exp[1:],color=S3exp_color,linestyle='dotted')
#
ax2.set_ylim((-1,60))
ax2.set_ylabel('Daily number of groups',fontsize=fs+1)
ax2.axvline(tc_S2+1,color='k',linestyle=':')
ax2.axvline(tc_S3+1,color='k',linestyle=':')
ax2.set_xticks(signal.index.values)
ax2.set_xticklabels(dates,rotation=35,ha='right',fontsize=fs)
ax2.locator_params(axis='x', nbins=10)
ax2.legend(['Daily nº of groups','Best fit '+r'$\Phi_2$'+' (power-law)','Best fit '+r'$\Phi_3$'+' (power-law)','Best fit '+r'$\Phi_3$'+' (exponential)'])
ax2.text(tc_S2-10,55.5,r'$\Phi_1$',fontsize=fs)
ax2.text(0.5*(tc_S2+tc_S3)-2,55.5,r'$\Phi_2$',fontsize=fs)
ax2.text(tc_S3+5,55.5,r'$\Phi_3$',fontsize=fs)
scS2=ax2.text(2,15,r'A='+'%.2f'%Ctheta_S2[0]+'\n'+r'$\theta=$'+'%.2f'%Ctheta_S2[1],color=S2scr_color)
scS2.set_bbox(dict(facecolor=S2scr_color, alpha=0.1, edgecolor=S2scr_color))
scS3=ax2.text(26,33,r'A='+'%.2f'%Ctheta_S3[0]+'\n'+r'$\theta=$'+'%.2f'%Ctheta_S3[1],color=S3scr_color)
scS3.set_bbox(dict(facecolor=S3scr_color, alpha=0.1, edgecolor=S3scr_color))
exS3=ax2.text(67,14,r'B='+'%.2f'%Calpha_S3[0]+'\n'+r'$\alpha=$'+'%.2f'%Calpha_S3[1],color=S3exp_color)
exS3.set_bbox(dict(facecolor=S3exp_color, alpha=0.1, edgecolor=S3exp_color))
ax2.text(0.7*signal.size,61*0.6,
         'Power law:\n\n'+ r"$f(x)=F'(x)=A\left(t-t_c \right)^{\theta-1}$",color='blue')
ax2.text(0.7*signal.size,61*0.4,
         'Exponential:\n\n'+r"$f(x)=F'(x)=B\alpha e^{-\alpha (t-t_c)}$",color='red')
ax2.text(tc_S2-3,61*0.65,'23 Oct Shock',rotation=90)
ax2.text(tc_S3-3,61*0.65,'17 Nov Shock',rotation=90)
plt.tight_layout()
plt.savefig('Figures/exogenous_shock.pdf')
plt.clf()
plt.close()

#################################################
# INSPECTING GROUPS CREATED IN S2 AND S3        #
#################################################


N_g_S2 = len(g_before_17nov2018) # 256
N_g_S3 = len(g_after_17nov2018) # 636

S2_mask = groups_geo_df.account_id.isin(g_before_17nov2018)
S3_mask = groups_geo_df.account_id.isin(g_after_17nov2018)

S2_groups_scales = groups_geo_df.loc[S2_mask,'urban_scale'].value_counts().sort_index()[1:]/N_g_S2
S3_groups_scales = groups_geo_df.loc[S3_mask,'urban_scale'].value_counts().sort_index()[1:]/N_g_S3

S2_groups_scales.sum()

urban_scale_labels = ['2K-5K','5K-10K','10K-20K','20K-50K','50K-100K','100K-200K','200K-2M','Nat./Cap.']

width=0.4
fig = plt.figure(figsize=(6, 4))
# axis 1: cummulative
ax=fig.add_subplot(1,1,1)
ax.bar(S2_groups_scales.index-width/2,S2_groups_scales.values,label='S2',width=width)
ax.bar(S3_groups_scales.index+width/2,S3_groups_scales.values,label='S3',width=width)
ax.set_xticks(S3_groups_scales.index.values)
ax.set_xticklabels(urban_scale_labels,rotation=45,ha='right')
ax.legend(['Before Acte I','After Acte I'])
ax.yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1,decimals=0))
plt.tight_layout()
plt.savefig('Figures/urban_scales_groups_S2_S3.pdf')
plt.clf()
plt.close()
    
#################################################
# ONLINE ACTIVITY AROUND ACTE I                 #
#################################################

period='W'
for dg in [4]:
    total_groups = lg_dg[dg].set_index('date').account_id.resample(period).nunique()
    total_posts = lg_dg[dg]['date'].value_counts().resample(period).sum()
    total_comments = lg_dg[dg].set_index('date').actual_comment_count.resample(period).sum()
    total_likes = lg_dg[dg].set_index('date').actual_like_count.resample(period).sum()
    total_shares = lg_dg[dg].set_index('date').actual_share_count.resample(period).sum()
    series_length = total_groups.size
    s={}
    for f in flag_cols:
        s[f]={}
        # groups
        s[f]['G']=lg_dg[dg][lg_dg[dg][f]].set_index('date').account_id.resample(period).nunique().divide(total_groups)#.plot(ax=ax['G'])
        # posts
        s[f]['P']=lg_dg[dg][lg_dg[dg][f]]['date'].value_counts().resample(period).sum().divide(total_posts)#.plot(ax=ax['P'])
        # comments
        s[f]['C']=lg_dg[dg][lg_dg[dg][f]].set_index('date').actual_comment_count.resample(period).sum().divide(total_comments)#.plot(ax=ax['C'])
        # likes
        s[f]['L']=lg_dg[dg][lg_dg[dg][f]].set_index('date').actual_like_count.resample(period).sum().divide(total_likes)#.plot(ax=ax['L'])
        # share
        s[f]['S']=lg_dg[dg][lg_dg[dg][f]].set_index('date').actual_share_count.resample(period).sum().divide(total_shares)#.plot(ax=ax['L'])
    pretty_titles={'G':'% of Active Groups','P':'% of Posts','C':'% of Comments','L':'% of Likes','S':'% of Shares'}
    flag_colors = ['tab:blue','indianred','tab:orange','tab:green','tab:purple','tab:brown']
    axvline_color = 'red'
    string_dates = ['%d-%d-%d'%(ix.year,ix.month,ix.day) for ix in total_groups.index]
    dates = []
    if period=='1d':
        for d in total_groups.index:
            if d.day==1:# or d.day==15:# or d.day==calendar.monthrange(d.year, d.month)[1]:
                dates.append('%d-%d'%(d.year,d.month))
            else:
                dates.append('')
    else:
        last_month = '0'
        for d in string_dates:
            if d.split('-')[1]!=last_month:
                appendable=d
                last_month=d.split('-')[1]
            else:
                appendable=''
            dates.append(appendable)
    
    for i,d in enumerate(string_dates):
        if pd.Timestamp(d)>=pd.Timestamp('2018-11-17'):
            ddaypos=i
            break
    # dates= string_dates
    custom_legend = []
    for f in flag_cols:
        if f=='F_logistic':
            custom_legend.append(Line2D([0], [0], color=flag_colors[0], lw=3.5,alpha=1.0,  label='Mobilization'))
            custom_legend.append(Line2D([0], [0], color='white', lw=2,alpha=1.0,         label=' '))
        elif f=='F_gj':
            custom_legend.append(Line2D([0], [0], color=flag_colors[1], lw=3.5,alpha=1.0,  label='Yellow Vests'))
            custom_legend.append(Line2D([0], [0], color='white', lw=2,alpha=1.0,         label='movement'))
        elif f=='F_precarity':
            custom_legend.append(Line2D([0], [0], color=flag_colors[2], lw=3.5, alpha=1.0,  label='Pracariousness'))
            custom_legend.append(Line2D([0], [0], color='white',        lw=2, alpha=1.0,  label='& life conditions'))
        elif f=='F_ric':
            custom_legend.append(Line2D([0], [0], color=flag_colors[3], lw=3.5,alpha=1.0,  label='Referendum '))
            custom_legend.append(Line2D([0], [0], color='white',        lw=2,alpha=1.0,  label='& RIC '))
        elif f=='F_fuel':
            custom_legend.append(Line2D([0], [0], color=flag_colors[4], lw=3.5,alpha=1.0,  label='Fuel tax'))
            # custom_legend.append()
    
    quantities_to_plot = ['G','P','C','L','S']
    fig = plt.figure(figsize=(12, 8))
    ax={}
    for i,q in enumerate(quantities_to_plot):
        ax[q]=fig.add_subplot(len(quantities_to_plot),1,i+1)  
        for f in flag_cols:
            # groups
            ax[q].plot(range(series_length),s[f][q].values,color=flag_colors[flag_cols.index(f)],linewidth=2.5)        
        # axvline
        if period=='1d':
            pass
        elif period=='W':
            ax[q].axvline(string_dates.index('2018-10-21'),color='k',linewidth=2,alpha=1,linestyle=':')
            ax[q].axvline(string_dates.index('2018-11-18'),color='k',linewidth=2,alpha=1,linestyle=':')
            ax[q].axvline(string_dates.index('2019-1-20'),color='k',linewidth=2,alpha=1,linestyle=':')
            
    # Group axes
    ax['G'].set_yticks([0,0.5,1])
    ax['G'].set_yticklabels(['0%','50%','100%'],fontsize=14)
    for a in ['G','P','C','L','S']:
        ax[a].set_xlabel('')
        ax[a].set_ylabel('')
        ax[a].set_ylim((0,1))
        ax[a].set_title(pretty_titles[a],fontsize=14)
        ax[a].set_xticks(range(series_length))
        ax[a].set_xticklabels(dates,rotation = 15, ha="right",fontsize=14)
    # Other axes
    for a in ['P','C','L','S']:
        ax[a].set_yticks([0,0.1,0.20,0.30])
        ax[a].set_yticklabels(['0%','10%','20%','30%'],fontsize=14)
        ax[a].set_ylim((0,0.3))
    ax['P'].set_ylim((0,0.2))
    for q in quantities_to_plot:
        if q!=quantities_to_plot[-1]:
            ax[q].set_xticklabels(['' for xtl in ax[q].get_xticklabels()])
    ax[quantities_to_plot[-1]].legend(handles=custom_legend,
                  loc='lower center', bbox_to_anchor=(0.5, -1.8),fontsize=14,ncol=5)
    #
    ax[q].text(0.4*string_dates.index('2018-10-21'),0.22,r'$\Phi_1$',fontsize=14)
    ax[q].text(0.45*(string_dates.index('2018-10-21')+string_dates.index('2018-11-18')),0.22,r'$\Phi_2$',fontsize=14)
    ax[q].text(0.45*(string_dates.index('2018-11-18')+string_dates.index('2019-1-20')),0.22,r'$\Phi_3$',fontsize=14)
    ax[q].text(0.5*(string_dates.index('2019-1-20')+len(string_dates)-16),0.22,r'$\Phi_4$',fontsize=14)
    #
    plt.tight_layout()
    plt.savefig('Figures/flag_timeseries_%s_v2.pdf'%(period))
    plt.clf()
    plt.close()  

############################################################
# TIME SERIES OF ACTES WITH PRESENT GROUPS AND PROTESTERS  #
############################################################

# smoothing graphics
n=100
k=3

# Pretty picture: timeseries actes groups
custom_legend=[ Line2D([0],[0],color='red',lw=2,alpha=1.0, label=  '[Sat.] Protesters '),
                Line2D([0],[0],color='blue',lw=2,alpha=1.0, label= '[Mon-Fri]  Active groups'),
                Line2D([0],[0],color='green',lw=2,alpha=1.0, label='[Mon-Fri]  Act. groups (mobilization)'),
                Line2D([0],[0],color='orange',lw=2,alpha=1.0, label= '[Mon-Fri]  Act. groups (YV mov.)'),]
fig = plt.figure(figsize=(6, 4))
ax = fig.add_subplot(1,1,1)
x=emb_actes.acte.values
y=emb_actes.people.values
xnew,ynew = spliner(x,y,n,k)
ax.plot(xnew,ynew,color='red',linewidth=2)
axt = ax.twinx()
y=emb_actes.G_All.values
xnew,ynew = spliner(x,y,n,k)
axt.plot(xnew,ynew,color='blue',linewidth=2)
y=emb_actes.G_F_logistic.values
xnew,ynew = spliner(x,y,n,k)
axt.plot(xnew,ynew,color='green',linewidth=2)
y=emb_actes.G_F_gj.values
xnew,ynew = spliner(x,y,n,k)
axt.plot(xnew,ynew,color='orange',linewidth=2)
# axes
ax.set_xlim((1,emb_actes.acte.max()))
ax.set_ylabel('Participants')
axt.set_ylabel('Active Groups')
ax.set_yticks([0,50000,100000,150000,200000,250000,300000])
ax.set_yticklabels(['0K','50K','100K','150K','200K','250K','300K'])
ax.set_xticks(emb_actes.acte.values)
ax.set_xticklabels([(d) for d in emb_actes.date],rotation=25,ha='right')
axb=ax.twiny()
axb.set_xlim(ax.get_xlim())
axb.set_xticks(emb_actes.acte.values)
axb.set_xticklabels(actes_df.roman.values,rotation=25,ha='left')
# legends
ax.legend(handles=custom_legend,loc='center right',fontsize=7,
         bbox_to_anchor=(0.99,0.26))
for n, label in enumerate(ax.xaxis.get_ticklabels()):
    if n not in [0,4,9,14,19,24,29,34]:
        label.set_visible(False)
for n, label in enumerate(axb.xaxis.get_ticklabels()):
    if n not in [0,4,9,14,19,24,29,34]:
        label.set_visible(False)
ax.axvline(10,color='k',linestyle=':')
ax.text(7,125000,r'$\Phi_3$',fontsize=11)
ax.text(12,125000,r'$\Phi_4$',fontsize=11)
plt.tight_layout()
plt.savefig('Figures/actes_groups.pdf')
plt.clf()
plt.close()

############################################
# CREATING SIGNALS OF SHORT TERM DYNAMICS  #
############################################

# creating signals
def line(x,a,b):
    return a+b*x;
cutoff=10
for q in ['G','P','C','L','S']:
    for f in flag_cols+['All']:
        col = '%s_%s'%(q,f)
        y = emb_actes[col].values[cutoff:]
        x = np.linspace(0,y.size-1,y.size)
        popt,_ = curve_fit(line, x, y)
        y_line = line(x,popt[0],popt[1])
        delta_mean= (y-y_line).mean()
        delta_factor = 1.0/np.abs((y-y_line-delta_mean)).max()
        x_prep = np.linspace(-emb_actes.shape[0]+x.size,-1,num=emb_actes.shape[0]-x.size)
        x_total = np.append(x_prep,x)
        y_line_total = line(x_total,popt[0],popt[1])
        emb_actes[col+'_signal']=delta_factor*(emb_actes[col].values-y_line_total-delta_mean)
        emb_actes[col+'_line']=y_line_total
col = 'people'
y = emb_actes[col].values[cutoff:]
x = np.linspace(0,y.size-1,y.size)
popt,_ = curve_fit(line, x, y)
y_line = line(x,popt[0],popt[1])
delta_mean= (y-y_line).mean()
delta_factor = 1.0/np.abs((y-y_line-delta_mean)).max()
x_prep = np.linspace(-emb_actes.shape[0]+x.size,-1,num=emb_actes.shape[0]-x.size)
x_total = np.append(x_prep,x)
y_line_total = line(x_total,popt[0],popt[1])
emb_actes[col+'_signal']=delta_factor*(emb_actes[col].values-y_line_total-delta_mean)
emb_actes[col+'_line']=y_line_total

############################################################
# GRANGER CAUSALITY TESTS OF SHORT TERM DYNAMICS           #
############################################################
# Granger-causality tests
# All test results, dictionary keys are the number of lags. 
# For each lag the values are a tuple, with the first element a 
# dictionary with test statistic, pvalues, degrees of freedom, 
# the second element are the OLS estimation results for the restricted model, 
# the unrestricted model and the restriction (contrast) matrix for the parameter f_test.



actes_cols = ['G_All', 'P_All', 'C_All', 'L_All', 'S_All',
              'G_F_logistic', 'P_F_logistic', 'C_F_logistic', 'L_F_logistic','S_F_logistic',
              'G_F_gj', 'P_F_gj', 'C_F_gj', 'L_F_gj', 'S_F_gj',
              'G_F_precarity','P_F_precarity', 'C_F_precarity', 'L_F_precarity', 'S_F_precarity',
              'G_F_ric', 'P_F_ric','C_F_ric', 'L_F_ric', 'S_F_ric',
              'G_F_fuel', 'P_F_fuel', 'C_F_fuel', 'L_F_fuel','S_F_fuel']
cutoff=10
signal_string='_signal'
test='ssr_ftest' # ssr_ftest, ssr_chi2test, lrtest, params_ftest
df_granger_actes = pd.DataFrame()
df_granger_actes['var']=actes_cols
granger_p_values = []
pearson_r_values=[]
granger_f_values=[]
for col in actes_cols:
    X=emb_actes[['people'+signal_string,col+signal_string]].values[cutoff:]
    X2=np.zeros((X.shape[0]-1,2))
    X2[:,0]=X[1:,0]
    X2[:,1]=X[:-1,1]
    result=granger(X2,[1],verbose=False)
    granger_p_values.append(result[1][0][test][1])
    granger_f_values.append(result[1][0][test][0])
    pearson_r_values.append(1.0-pearsonr(X[:,0],X[:,1])[0])
df_granger_actes['p_values'] = granger_p_values
df_granger_actes['f_values'] = granger_f_values
df_granger_actes['r_values'] = pearson_r_values
# creating heatmap

granger_heatmap_df = pd.DataFrame(columns=flag_cols,
                                  index=['G','P','C','L'])
granger_F_heatmap_df = pd.DataFrame(columns=flag_cols,
                                  index=['G','P','C','L'])
for i in ['G','P','C','L','S']:
    for j in flag_cols:
        granger_heatmap_df.loc[i,j]=float(df_granger_actes[df_granger_actes['var']=='%s_%s'%(i,j)].p_values.iloc[0])
for i in ['G','P','C','L','S']:
    for j in flag_cols:
        granger_F_heatmap_df.loc[i,j]=float(df_granger_actes[df_granger_actes['var']=='%s_%s'%(i,j)].f_values.iloc[0])
M=np.array(granger_heatmap_df.values,dtype=float)
M_F=np.array(granger_F_heatmap_df.values,dtype=float)
M2=M
fs=18
lw=2.5
fig = plt.figure(figsize=(12, 10))        
ax = plt.subplot(1,1,1)
im=ax.matshow(M2,cmap=plt.cm.Blues,vmin=0,vmax=M.max())
ax.set_xticklabels(['','Mobilization','YV Movement','Precariousness','Ref. & RIC','Fuel Tax'],fontsize=fs-4,rotation=0,ha='center', rotation_mode="anchor")
ax.set_yticklabels(['','Groups\n','Posts\n','Comments\n','Likes\n','Shares\n'],fontsize=fs-2,rotation=90,ha='center', rotation_mode="anchor")
for j in range(5):
    for i in range(5):
        if M[j,i]<0.5*M.max():
            if M[j,i]<0.05:
                ax.text(i-0.35,j+0.00,'p=%.3f'%M[j,i],color='red',fontsize=fs)
                ax.text(i-0.25,j+0.25,'F=%.1f'%M_F[j,i],color='red',fontsize=fs)
            else:
                ax.text(i-0.35,j+0.00,'p=%.3f'%M[j,i],color='black',fontsize=fs)
                ax.text(i-0.25,j+0.25,'F=%.1f'%M_F[j,i],color='black',fontsize=fs)
        else:
            ax.text(i-0.35,j+0.00,'p=%.3f'%M[j,i],color='white',fontsize=fs)
            ax.text(i-0.25,j+0.25,'F=%.1f'%M_F[j,i],color='white',fontsize=fs) 
        if M[j,i]<0.01:
            ax.plot([i-0.5,i-0.5],[j-0.5,j+0.5],color='red',linewidth=lw)
            ax.plot([i+0.5,i+0.5],[j-0.5,j+0.5],color='red',linewidth=lw)
            ax.plot([i-0.5,i+0.5],[j-0.5,j-0.5],color='red',linewidth=lw)
            ax.plot([i-0.5,i+0.5],[j+0.5,j+0.5],color='red',linewidth=lw)
        elif M[j,i]<0.05:
            ax.plot([i-0.5,i-0.5],[j-0.5,j+0.5],color='red',linewidth=lw)
            ax.plot([i+0.5,i+0.5],[j-0.5,j+0.5],color='red',linewidth=lw)
            ax.plot([i-0.5,i+0.5],[j-0.5,j-0.5],color='red',linewidth=lw)
            ax.plot([i-0.5,i+0.5],[j+0.5,j+0.5],color='red',linewidth=lw)
ax.set_xlabel('\np-value for the Granger Causality Test',fontsize=18)
cbar = fig.colorbar(im, ticks=[0,0.05,M.max()])
cbar.ax.set_yticklabels(['0', '0.05 (*)', '%.2f'%M.max()],fontsize=fs)  # vertically oriented colorbar
# cbar.ax.plot([0, 1], [0.01, 0.01], 'red',linewidth=5)
cbar.ax.plot([0, 1], [0.05,0.05], 'red',linewidth=5)
cbar.set_label('p-value', rotation=270,fontsize=fs)
plt.savefig('Figures/granger_tests.pdf')
plt.clf()
plt.close()

############################################################
# ONLINE TIME SERIES OF SHORT TERM DYNAMICS                #
############################################################

n=100
k=3
cutoff = 10
fig = plt.figure(figsize=(9, 8))        
ax={}
flag_colors = ['tab:blue','tab:green','tab:purple','tab:brown','indianred','tab:orange',]
quantities=['G','P','C','L','S']
pretty_quantities={'G':'Groups','P':'Posts','C':'Comments','L':'Likes','S':'Shares'}
pretty_flags={'F_logistic':'Mobilization', 'F_gj':'YV Movement', 'F_precarity':'Precariousness', 'F_ric':'Ref. & RIC', 'F_fuel':'Fuel Tax'}
for i,f in enumerate(flag_cols):
    ax[f]={}
    for j,q in enumerate(quantities):
        ax[f][q] = plt.subplot(len(quantities),len(flag_cols),1+i+j*len(flag_cols))
        x=np.linspace(0,emb_actes['%s_%s_signal'%(q,f)].values[cutoff:].size-1,emb_actes['%s_%s_signal'%(q,f)].values[cutoff:].size)
        y=emb_actes['%s_%s_signal'%(q,f)].values[cutoff:]
        xnew,ynew = spliner(x,y,n,k)
        ax[f][q].plot(xnew,ynew,
                   color='blue',linewidth=1)
        x=np.linspace(0,emb_actes['people_signal'].values[cutoff:].size-1,emb_actes['people_signal'].values[cutoff:].size)
        y=emb_actes['people_signal'].values[cutoff:]
        xnew,ynew = spliner(x,y,n,k)
        ax[f][q].plot(xnew,ynew,
                   color='red',linewidth=1,linestyle='--')
        if i!=0:
            ax[f][q].set_yticks([], [])
        if j!=(len(quantities)-1):
            ax[f][q].set_xticks([], [])
        if j==(len(quantities)-1):
            mask=[0,4,9,14,19]
            ax[f][q].set_xticks((actes_df.acte.values[cutoff:]-cutoff-1)[mask])
            ax[f][q].set_xticklabels(actes_df.roman.values[cutoff:][mask],rotation=90,fontsize=14)
        if i==0:
            ax[f][q].set_ylabel(pretty_quantities[q],fontsize=14)
        if j==0:
            ax[f][q].set_title(pretty_flags[f],fontsize=14)
        ax[f][q].set_ylim((-1.5,1.5))    
plt.savefig('Figures/time_signals.pdf')
plt.clf()
plt.close()


############################################################
# ONLINE TIME SERIES AND REGRESSIONS OF LONG TERM DYNAMICS #
############################################################

n=100
k=3

cutoff = 0
fig = plt.figure(figsize=(10, 10))        
ax={}
flag_colors = ['tab:blue','tab:green','tab:purple','tab:brown','indianred','tab:orange',]
quantities=['G','P','C','L','S']
pretty_quantities={'G':'Groups','P':'Posts','C':'Comments','L':'Likes','S':'Shares'}
pretty_flags={'F_logistic':'Mobilization', 'F_gj':'YV Movement', 'F_precarity':'Precariousness', 'F_ric':'Ref. & RIC', 'F_fuel':'Fuel Tax'}
for i,f in enumerate(flag_cols):
    ax[f]={}
    for j,q in enumerate(quantities):
        ax[f][q] = plt.subplot(len(quantities),len(flag_cols),1+i+j*len(flag_cols))
        x=np.linspace(0,emb_actes['%s_%s'%(q,f)].values[cutoff:].size-1,emb_actes['%s_%s'%(q,f)].values[cutoff:].size)
        y=emb_actes['%s_%s'%(q,f)].values[cutoff:]
        popt,pcov=curve_fit(linfunc,x,y)
        perr = np.sqrt(np.diag(pcov))
        ss_res = np.sum((y-linfunc(x,*popt))**2)
        ss_tot = np.sum((y-np.mean(y))**2)
        R2 = 1-(ss_res/ss_tot)
        xnew,ynew = spliner(x,y,n,k)
        ax[f][q].plot(xnew,ynew,color='blue',linewidth=1)
        ax[f][q].plot(range(emb_actes['%s_%s_line'%(q,f)].values[cutoff:].size),
                   emb_actes['%s_%s_line'%(q,f)].values[cutoff:],
                   color='green',linewidth=2.5,alpha=0.9,linestyle='-')
        ax[f][q].axvspan(9, 32, facecolor='green', alpha=0.15)
        ax[f][q].axvline(9,color='green',linewidth=2,linestyle=':')
        # ax[f][q].text(12,0.9*ynew.max(),r'$R^2$'+'=%.2f'%R2,color='green',fontsize=13)
        if i!=0:
            ax[f][q].set_yticks([], [])
        if j!=(len(quantities)-1):
            ax[f][q].set_xticks([], [])
        if j==(len(quantities)-1):
            mask=[0,4,9,14,19,24,29]
            ax[f][q].set_xticks((actes_df.acte.values[cutoff:]-cutoff-1)[mask])
            ax[f][q].set_xticklabels(actes_df.roman.values[cutoff:][mask],rotation=90,fontsize=14)
        if i==0:
            ax[f][q].set_ylabel(pretty_quantities[q],fontsize=15)
            if q!='G':
                ax[f][q].set_yticklabels(['{:,.0f}'.format(x) + 'K' for x in ax[f][q].get_yticks()/1000])
            else:
                ax[f][q].set_yticklabels(['{:,.1f}'.format(x) + 'K' for x in ax[f][q].get_yticks()/1000])
        if j==0:
            ax[f][q].set_title(pretty_flags[f],fontsize=15)
        ax[f][q].set_ylim((0,1.1*ynew.max()))
        ax[f][q].set_xlim((0,32))
plt.savefig('Figures/time_signals_linear_reg.pdf')
plt.clf()
plt.close()

#############################################################
# OFFLINE TIME SERIES AND REGRESSIONS OF LONG TERM DYNAMICS #
#############################################################

n=100
k=2

fig = plt.figure(figsize=(8, 4))        
ax={'regression':plt.subplot(1,2,1),
    'signal':plt.subplot(1,2,2),}
x=np.linspace(0,emb_actes['people'].size-1,emb_actes['people'].size)
y=emb_actes['people'].values
xnew,ynew = spliner(x,y,n,k)
ax['regression'].plot(xnew,ynew,color='red')
ax['regression'].plot(range(emb_actes['people_line'].size),
                      emb_actes['people_line'],
                      color='green',linewidth=2,alpha=0.75,linestyle='-')
ax['regression'].axvspan(9, 32, facecolor='green', alpha=0.25)
ax['regression'].axvline(9,color='green',linewidth=2,linestyle=':')
mask=[0,4,9,14,19,24,29]
ax['regression'].set_xticks((actes_df.acte.values-1)[mask])
ax['regression'].set_xticklabels(actes_df.roman.values[mask],rotation=90,fontsize=14)
ax['regression'].set_yticklabels(['{:,.0f}'.format(x) + 'K' for x in ax['regression'].get_yticks()/1000])
ax['regression'].text(5.5,200000,'S3',fontsize=14)
ax['regression'].text(10,200000,'S4',fontsize=14)
ax['regression'].legend(['Number of protesters','Slow dynamics (S4)'])
ax['regression'].set_title('Number of protesters in S3 ans S4\n(fitted linear slow dynamic in S4)')
cutoff = 10
x=np.linspace(0,emb_actes['people_signal'].values[cutoff:].size-1,emb_actes['people_signal'].values[cutoff:].size)
y=emb_actes['people_signal'].values[cutoff:]
xnew,ynew = spliner(x,y,n,k)
ax['signal'].plot(xnew,ynew,
                    color='red',linewidth=1)
mask=[0,4,9,14,19]
ax['signal'].set_xticks((actes_df.acte.values[cutoff:]-cutoff-1)[mask])
ax['signal'].set_xticklabels(actes_df.roman.values[cutoff:][mask],rotation=90,fontsize=14)
ax['signal'].legend(['Normalized fast dynamics (S4)'])
ax['signal'].set_title('Normalized fast dynamics\n of protesters during S4')
plt.tight_layout()
plt.savefig('Figures/time_signals_offline_linear_reg.pdf')
plt.clf()
plt.close()

n=100
k=3
fig = plt.figure(figsize=(5, 5))        
ax={'regression':plt.subplot(1,1,1)}
left, bottom, width, height = 0.47,0.45,0.45,0.3
ax['signal']= fig.add_axes([left, bottom, width, height])
x=np.linspace(0,emb_actes['people'].size-1,emb_actes['people'].size)
y=emb_actes['people'].values
popt,pcov=curve_fit(linfunc,x,y)
perr = np.sqrt(np.diag(pcov))
ss_res = np.sum((y-linfunc(x,*popt))**2)
ss_tot = np.sum((y-np.mean(y))**2)
R2 = 1-(ss_res/ss_tot)
perr[1]/np.abs(popt[1])
xnew,ynew = spliner(x,y,n,k)
ax['regression'].plot(xnew,ynew,color='red')
ax['regression'].plot(range(emb_actes['people_line'].size),
                      emb_actes['people_line'],
                      color='green',linewidth=2,alpha=0.75,linestyle='-')
ax['regression'].axvspan(9, 32, facecolor='green', alpha=0.25)
ax['regression'].axvline(9,color='green',linewidth=2,linestyle=':')
mask=[0,4,9,14,19,24,29]
ax['regression'].set_xticks((actes_df.acte.values-1)[mask])
ax['regression'].set_xticklabels(actes_df.roman.values[mask],rotation=90,fontsize=14)
ax['regression'].set_yticklabels(['{:,.0f}'.format(x) + 'K' for x in ax['regression'].get_yticks()/1000])
ax['regression'].text(5.5,275e3,r'$\Phi_3$',fontsize=14)
ax['regression'].text(9.5,275e3,r'$\Phi_4$',fontsize=14)
ax['regression'].legend(['Protesters ('+r'$\Phi_4$'+'): '+r'f$(t)$','Slow dynamics ('+r'$\Phi_4$'+'): '+r'$f_S (t)$'])
ax['regression'].set_xlim((0,32))
cutoff = 10
x=np.linspace(0,emb_actes['people_signal'].values[cutoff:].size-1,emb_actes['people_signal'].values[cutoff:].size)
y=emb_actes['people_signal'].values[cutoff:]
xnew,ynew = spliner(x,y,n,k)
ax['signal'].plot(xnew,ynew,color='red',linewidth=1,linestyle='--')
mask=[0,4,9,14,19]
ax['signal'].set_xticks((actes_df.acte.values[cutoff:]-cutoff-1)[mask])
ax['signal'].set_xticklabels(actes_df.roman.values[cutoff:][mask],rotation=90,fontsize=12)
ax['signal'].set_xlim((0,22))
ax['signal'].set_ylim((-1,1))
ax['signal'].axhline(0,color='k',alpha=0.25)
ax['signal'].text(9,0.6,r'$\hat{f}_F(t)$')
ax['signal'].set_title('Normalized fast dynamics',fontsize=10)
plt.tight_layout()
plt.savefig('Figures/time_signals_offline_linear_reg_bis.pdf')
plt.clf()
plt.close()

    
    
    
    
    