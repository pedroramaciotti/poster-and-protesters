# Online-Offline Feedback in Social Protests


These are the data and the reproducibility script for the article "Posters and protesters: The networked interplay between onsite participation and Facebook activity in the Yellow Vests movement in France ", by Pedro Ramaciotti Morales, Jean-Philippe Cointet, and Caterina Froio.

## Download the data

All the data needed is included in a single zip file downloadable from [Figshare](https://figshare.com/articles/dataset/Online-offline_activity_feedback_in_social_movements/14790408).

## Run the script

A single script reproduces all the analyses included in the article and outputs the images: reproducibility.py

## Privacy

All data are pseudonymized upon collection. Shared texts, links, and other elements that might allow for the identification of Facebook accounts have been removed. 

Please refer to the article to check the details of GDPR compliance and the Data Management Plan.

